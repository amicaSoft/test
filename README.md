# storeTest

en este proyecto tenemos el Frontend en "/" y en la carpeta "server/" tenemos el backend para el consumo de la API

:construction: Proyecto en construcción:construction:

## :hammer: Funcionalidades del proyecto

- `SPA de productos`: muestra en la página inicial el catálogo de productos 1
- `Consumo de API`: los productos mostrados son consumidos de una API simple
- `Detalle de producto`: cada producto tiene acceso a sus detalles para revisar sus opciones de compra
- `Acciones de producto`: se muestra de forma simple las opciones de cada producto en su detalle
- `Carrito de compras`: se van agregando los productos a un carrito
- `Persistencia de datos`: se usa localStorage para mantener los datos consumidos de la API

## :heavy_check_mark: tecnologías utilizadas

- [Reactjs](https://es.reactjs.org/)
- [tailwind](https://tailwindcss.com/)
- [nodejs](https://nodejs.org)

## Los pasos simples para ejecutar el proyecto

1.  Descargamos el proyecto
2.  Ejecutamos "npm i" para las dependencias
3.  Ejecutamos "npm start", accedemos a localhost:3000
4.  Separamos la carpeta "server/"
5.  En server/ ejecutamos "npm i " para las dependencias
6.  Ejecutamos "npm start" para tener la API disponible en localhost:3001

## Scripts disponibles Frontend

En el proyecto puedes correr:

### `npm start`

Correr la app en modo desarrollador.\
Abrir [http://localhost:3000](http://localhost:3000) para revisar en el navegador.

hay algunos logs mostrados en consola a modo de pruebas

### `npm test`

Lanza un grupo de test generados

### `npm run build`

Construye la app para producción en la siguiente carpeta `build`
React empaqueta correctamente en modo de producción y optimiza la compilación para obtener el mejor rendimiento.

¡Estaria listo para desplegar!

### `npm run lint`

genera una revisión del código con una guía de estilo popular
