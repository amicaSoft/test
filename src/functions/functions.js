import axios from "axios";

const allProducts = async (state, stateBase) => {
  const res = await axios.get("http://localhost:3001/api/product");
  state(res.data);
  stateBase(res.data);
  window.localStorage.setItem("products", JSON.stringify(res.data));
  console.log(res.data);
};

const oneProduct = async (id, state) => {
  const res = await axios.get(`http://localhost:3001/api/product/${id}`);
  state(res.data[0]);
  console.log(res);
};

export { allProducts, oneProduct };
