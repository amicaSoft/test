import React from "react";
import BreadCrumb from "./BreadCrumb";
import { AiOutlineShoppingCart } from "react-icons/ai";

const Header = () => {
  return (
    <>
      <nav className="bg-white border-gray-200 px-2 sm:px-4 py-8 rounded dark:bg-gray-900">
        <div className="inline-block  ">
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7b/Meta_Platforms_Inc._logo.svg/799px-Meta_Platforms_Inc._logo.svg.png"
            className="h-6 mr-3 sm:h-9"
          />
        </div>
        <div className="inline-block float-right  ">
          <AiOutlineShoppingCart className="w-8 mr-6 sm:h-12" />
        </div>
      </nav>
      <BreadCrumb />
    </>
  );
};

export default Header;
