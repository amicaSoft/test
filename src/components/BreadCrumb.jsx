import React from "react";
import { useLocation, Link } from "react-router-dom";

const BreadCrumb = () => {
  const location = useLocation();
  const breadCrumbView = () => {
    const { pathname } = location;
    const pathnames = pathname.split("/").filter((item) => item);
    const capatilize = (s) => s.charAt(0).toUpperCase() + s.slice(1);
    return (
      <div>
        <nav className=" px-8 pb-6 mx-8 sm:px-4 rounded-md w-full">
          <ol className="list-reset flex">
            {pathnames.length > 0 ? (
              <>
                <li>
                  <Link className="text-blue-600 hover:text-blue-700" to="/">
                    Home
                  </Link>
                </li>
                <li>
                  <span className="text-gray-500 mx-2">/</span>
                </li>
              </>
            ) : (
              <li className="text-gray-500">Home</li>
            )}
            {pathnames.map((name, index) => {
              const routeTo = `/${pathnames.slice(0, index + 1).join("/")}`;
              const isLast = index === pathnames.length - 1;
              return isLast ? (
                <li className="text-gray-500" key={index}>
                  {capatilize(name)}
                </li>
              ) : (
                <div key={index}>
                  <li>
                    <Link
                      className="text-blue-600 hover:text-blue-700"
                      to={`${routeTo}`}
                    >
                      {capatilize(name)}{" "}
                    </Link>
                    <span className="text-gray-500 mx-2">/</span>
                  </li>
                </div>
              );
            })}
          </ol>
        </nav>
      </div>
    );
  };

  return <>{breadCrumbView()}</>;
};

export default BreadCrumb;
