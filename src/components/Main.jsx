import React, { useEffect, useState } from "react";
import { allProducts } from "../functions/functions";
import List from "./List";

const Main = () => {
  const [products, setProducts] = useState(null);
  const [baseProduct, setBaseProduct] = useState([]);

  const filter = (term) => {
    var result = baseProduct.filter((element) => {
      if (
        element.brand.toString().toLowerCase().includes(term.toLowerCase()) ||
        element.model.toString().toLowerCase().includes(term.toLowerCase())
      ) {
        return element;
      }
    });
    setProducts(result);
  };

  const timeStampVerify = (localProducts) => {
    const timeStamp = window.localStorage.getItem("timeStamp");
    if (timeStamp) {
      if (timeStamp + 60 * 60 * 1000 <= Date.now()) {
        allProducts(setProducts, setBaseProduct);
      }
    } else {
      window.localStorage.setItem("timeStamp", Date.now());
    }
  };

  useEffect(() => {
    const localProducts = window.localStorage.getItem("products");
    if (localProducts) {
      timeStampVerify(localProducts);
      setProducts(JSON.parse(localProducts));
    } else {
      allProducts(setProducts, setBaseProduct);
      window.localStorage.setItem("timeStamp", Date.now());
    }
  }, []);

  return (
    <>
      <div className=" min-w-screen min-h-screen bg-zinc-100 p-2 lg:p-10 ">
        <div className="mx-auto max-w-2xl py-5 px-4 sm:py-24 sm:px-6 lg:max-w-7xl lg:px-8">
          <div className="block ">
            <h2 className=" inline-block text-2xl font-bold tracking-tight text-gray-900">
              Productos
            </h2>

            <div className=" inline-block float-right text-gray-600 focus-within:text-gray-400 ">
              <svg
                fill="none"
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                className="w-6 h-6 inline-block"
              >
                <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
              </svg>

              <input
                type="text"
                className="form-control inline-block px-3 py-1.5 text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                placeholder="Buscar"
                onChange={(e) => {
                  filter(e.target.value);
                }}
              />
            </div>
          </div>
          <div className="mt-6 grid grid-cols-1 gap-y-10 gap-x-6 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
            <List products={products}></List>
          </div>
        </div>
      </div>
    </>
  );
};

export default Main;
