import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { oneProduct } from "../functions/functions";

const Detail = () => {
  const [product, setProduct] = useState(null);
  const { id } = useParams();
  const [cart, setCart] = useState({ id: "000" + id });

  const params = useParams();
  const addStorage = (e) => {
    setCart(Object.assign(cart, { storageCode: e.target.value }));
    console.log(cart);
  };

  const addColor = (e) => {
    setCart(Object.assign(cart, { colorCode: e.target.value }));
    console.log(cart);
  };

  useEffect(() => {
    oneProduct(params.id, setProduct);
    console.log(id);
  }, []);

  useEffect(() => {
    oneProduct(params.id, setProduct);
  }, []);

  return (
    <>
      {product != null ? (
        <div className="min-w-screen min-h-screen bg-zinc-200 flex items-center p-5 lg:p-10 overflow-hidden relative">
          <div className="w-full max-w-6xl rounded bg-white shadow-xl p-10 lg:p-20 mx-auto text-gray-800 relative md:text-left">
            <div className="md:flex items-center -mx-10">
              <div className="w-full md:w-1/2 px-10 mb-10 md:mb-0">
                <div className="relative">
                  <img
                    src={product.imgUrl}
                    className="w-full relative z-10"
                    alt=""
                  />
                  <div className="border-4 bg-indigo-300 absolute top-10 bottom-10 left-10 right-10 z-0"></div>
                </div>
              </div>
              <div className="w-full md:w-1/2 px-10">
                <div className="mb-6">
                  <h1 className="font-bold uppercase text-2xl mb-5">
                    {product.brand} <br />
                    Waterproof Jacket
                  </h1>
                  <p className="text-sm">detalle producto </p>
                </div>
                <div>
                  <div className="block align-bottom mr-5">
                    <span className="text-2xl leading-none align-baseline">
                      $
                    </span>
                    <span className="font-bold text-5xl leading-none align-baseline">
                      {product.price}
                    </span>
                    <span className="text-2xl leading-none align-baseline">
                      .00
                    </span>
                  </div>
                  <div className="mt-2">
                    <h3>RAM:</h3>
                  </div>
                  <div className="flex align-bottom my-3">
                    {product.options.storage.map((data) => (
                      <button key={data.name}>
                        <input
                          className="hidden"
                          type="radio"
                          id={data.name}
                          name="row1"
                          value={data.code}
                          onChange={addStorage}
                        />
                        <label
                          className=" mr-2 transition ease-in-out duration-300 delay-150 hover:text-blue-500 hover:border-blue-500 rounded-lg px-4 py-2 mt-4  font-semibold border-2 border-gray-300 text-gray-300"
                          htmlFor={data.name}
                        >
                          {data.name}
                        </label>
                      </button>
                    ))}
                  </div>
                  <div className="mt-2">
                    <h3>COLOR:</h3>
                  </div>
                  <div className="flex align-bottom my-3">
                    {product.options.color.map((data) => (
                      <button key={data.code}>
                        <input
                          className="hidden"
                          type="radio"
                          id={data.name}
                          name="row2"
                          onChange={addColor}
                          value={data.code}
                        />
                        <label
                          className=" mr-2 transition ease-in-out duration-300 delay-150 hover:text-blue-500 hover:border-blue-500 rounded-lg px-4 py-2 mt-4  font-semibold border-2 border-gray-300 text-gray-300"
                          htmlFor={data.name}
                        >
                          <i className="mdi mdi-cart -ml-2 mr-2"></i>
                          {data.name}
                        </label>
                      </button>
                    ))}
                  </div>

                  <div className="block align-bottom mt-10">
                    <button className="bg-yellow-300 transition ease-in-out duration-300 delay-150 opacity-75 hover:opacity-100 text-yellow-900 hover:text-gray-900 rounded-lg px-10 py-2 font-semibold">
                      <i className="mdi mdi-cart -ml-2 mr-2"></i> AGREGAR
                    </button>
                    {!cart.storageCode && !cart.colorCode && (
                      <h3 className="text-red-600">
                        * tiene que marcar las especificaciones
                      </h3>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        "no hay producto"
      )}
    </>
  );
  // <div>articulo con id {params.id}</div>
};

export default Detail;
