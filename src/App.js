import { BrowserRouter, Routes, Route } from "react-router-dom";
import Detail from "./components/Detail";
import Main from "./components/Main";
import Header from "./components/Header";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Main></Main>}></Route>
          <Route path="/article/:id" element={<Detail></Detail>}></Route>
          <Route path="/article" element={<Main></Main>}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
